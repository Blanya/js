var prompt = require("prompt-sync")();
var age = Number(prompt("Donnez moi l'âge de votre enfant : "));

switch (true) {
    case (age<3):
        console.log("Votre enfant est trop jeune pour pratiquer");
        break;
    case age>=3 && age<=6:
        console.log("Votre enfant sera ds la catégorie Baby");
        break;
    case age==7 || age==8:
        console.log("Votre enfant sera ds la catégorie Poussin");
        break;
    case age==9 || age==10:
        console.log("Votre enfant sera ds la catégorie Pupille");
        break;
    case age==11 || age==12:
        console.log("Votre enfant sera ds la catégorie Minime");
        break;
    case age>=13 && age<18:
        console.log("Votre enfant sera ds la catégorie Cadet");
        break;
    default:
        console.log("Votre enfant est désormais un adulte!")
        break;
}