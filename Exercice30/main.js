var prompt = require("prompt-sync")();

let longueur, T=[];

longueur = Number(prompt("Combien de valeurs comportent le tableau ? "));

for (let i = 0; i < longueur; i++) 
{
    T[i]= (prompt("Entrez la valeur " + (i+1) + " de votre tableau : "));
}

console.log("Voici votre tableau " + T);

for (let i = 1; i < longueur ; i++) 
{
    let temp;
    temp = T[i];
    T[i] = T[i-1];
    T[i-1] = temp
}

console.log("Votre tableau avec la première valeur à la dernière place " + T);