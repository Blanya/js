var prompt = require("prompt-sync")();

let longueur, T=[];

longueur = Number(prompt("Combien de valeurs comportent le tableau ? "));

for (let i = 0; i < longueur; i++) 
{
    T[i]= Number(prompt("Entrez la valeur " + (i+1) + " de votre tableau : "));
}

console.log("Voici votre tableau : " + T)

let temp;
let i = 0;
j = T.length -1;
while(i<j)
{
    temp = T[j];
    T[j] = T[i];
    T[i] = temp;
    i++;
    j--;
}

console.log("Voici votre tableau inversé : " + T)