var prompt = require("prompt-sync")();

let longueur, T=[];
let tri = 0;

longueur = Number(prompt("Combien de valeurs comportent le tableau ? "));

for (let i = 0; i < longueur; i++) 
{
    T[i]= Number(prompt("Entrez la valeur " + (i+1) + " de votre tableau : "));
}

console.log("Voici votre tableau : " + T)

for (let i = 0; i < T.length-1; i++) 
{
    let j = i + 1;
    if(T[j]>T[i])
    {
        tri += 0;
    }
    if(T[j]<T[i])
    {
        tri += 1;
    }
}

if(tri === 0 )
{
    console.log("Votre tableau est trié par ordre croissant");
}
else 
{
    console.log("Votre tableau n'est pas trié");
}