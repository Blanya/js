console.log("La valeur de π est : " + Math.PI);

var prompt = require("prompt-sync")();

let diametre = Number(prompt("Donnez-moi le diamètre du cercle: "));

let perimetre = Number(Math.PI * diametre);
console.log("Périmètre = " + perimetre + "cm");

//let aire = Number((diametre/2) * (diametre/2) * Math.PI);
let aire = Number(Math.pow(diametre/2, 2) * Math.PI);
console.log("Aire = " + aire + "cm²");

console.log("Périmètre (arrondi) = " + Math.round(perimetre) + "cm");
console.log("Aire (arrondi) = " + Math.round(aire) + "cm²");