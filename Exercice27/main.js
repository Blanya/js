var prompt = require("prompt-sync")();

let jour, mois, annee, jj, mm, aa, nbAnnees, ecartAn = 0;

function verifJour(nb)
{
    while(nb>31 || nb<1)
    {
        nb = Number(prompt("Entrez le jour sous forme JJ : "));
    }
    return nb;
}

function verifMois(m)
{
    while(m>12 || m<1)
    {
        m = Number(prompt("Entrez le mois sous forme MM : "));
    }
    return m;
}

function verifAn(a)
{
    while(a<1870 || a>2100)
    {
        a = Number(prompt("Entrez l'année sous forme AAAA : "));
    }
    return a;
}

function verifDate  (moisAct, moisNaissance, jourAct, jourNaissance, ecartAnnees)
{
    let ecartM, ecartJ;
    ecartM = moisAct - moisNaissance;

    if(ecartM<0)
    {
        ecartAnnees = ecartAnnees - 1; 
    }
    else if(ecartM == 0)
    {
        ecartJ = jourAct - jourNaissance;
        if (ecartJ < 0)
        {
            ecartAnnees = ecartAnnees - 1;
        }
    }
    return ecartAnnees;
}
        

jour = Number(prompt("Entrez votre jour de naissance sous forme JJ : "));
verifJour(jour);

mois = Number(prompt("Entrez votre mois de naissance sous forme MM : "));
verifMois(mois);

annee = Number(prompt("Entrez votre année de naissance sous forme AAAA : "));
verifAn(annee);


jj = Number(prompt("Entrez le jour actuel sous forme JJ : "));
verifJour(jj);

mm = Number(prompt("Entrez le mois actuel sous forme MM : "));
verifMois(mm);

aa = Number(prompt("Entrez l'année actuelle sous forme AAAA : "));
verifAn(aa);

ecartAn = aa - annee; 

nbAnnees = verifDate(mm, mois, jj, jour, ecartAn);

if (nbAnnees >= 60) 
{
    console.log("Vous avez le droit à la carte sénior+ !");
} 
else 
{
    console.log("Vous n'avez pas le droit à la carte sénior");
}