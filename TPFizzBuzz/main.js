let btn_buzz = document.getElementById("btn_buzz");
let btn_moins = document.getElementById("btn_moins");
let btn_plus = document.getElementById("btn_plus");
let buzz_content = document.getElementById("btn_buzz-content");

btn_buzz.style.width = "170px";
btn_buzz.style.height = "170px";
btn_moins.style.cssText =
`
top : 148px;
right : 55vw;
width : 100px;
height : 100px;
`
btn_plus.style.cssText =
`
top : 100px;
right : 50px;
width : 100px;
height : 100px;
`

var val = 1;

function augmentation()
{
    val ++;
    buzz_content.textContent = val;
    fizzBuzz();
}

function diminution()
{
    val --;
    buzz_content.textContent = val;
    fizzBuzz();
}

function fizzBuzz()
{
    if(val%3 == 0)
    {
        buzz_content.className = "text-primary pt-5";
        buzz_content.textContent = "Buzz"; 
        
        if(val%5 == 0)
        {
            buzz_content.className = "text-warning pt-5";
            buzz_content.textContent = "FizzBuzz";     
        }
    }
    else if(val%5 == 0)
    {
        buzz_content.className = "text-success pt-5";
        buzz_content.textContent = "Fizz"; 
    }
    else
    {
        buzz_content.className = "pt-5 ";
    }
        
}
