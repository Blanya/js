var prompt = require("prompt-sync")();

let sommeDue, prixAchats = 0, achat, sommeDonnee, erreur;

do {
    achat = Number(prompt("Combien coûte votre article ? "));
    prixAchats += achat;
    console.log("La somme totale de vos achats pour le moment est de : " + prixAchats + " euros");
} while (((prixAchats%10) != 0) || (achat != 0));

sommeDonnee = Number(prompt("Combien donnez-vous ? "));

while(sommeDonnee<prixAchats)
{
    console.log("Vous ne m'avez pas donnée assez, vous m'avez donné " + sommeDonnee + " euros");
    erreur =  Number(prompt("Veuillez ajouter de l'argent svp, combien donnez-vous en plus? "));
    sommeDonnee = sommeDonnee + erreur;
}

sommeDue = sommeDonnee - prixAchats;

if(sommeDue == 0)
{
    console.log("Le compte est bon");
}
else
{
    while(sommeDue>=10)
    {
        sommeDue = sommeDue - 10;
        console.log("10 euros");
    }

    if(sommeDue>=5)
    {
        sommeDue = sommeDue - 5;
        console.log("5 euros");
    }
    else 
    {
        while(sommeDue>=1)
        {
            sommeDue = sommeDue -1;
            console.log("1 euros");
        }
    }
}

