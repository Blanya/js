const prompt = require("prompt-sync")();

function convert(heure, minutes, secondes)
{
    conversion = heure * 3600 + minutes * 60 + secondes;
    return conversion;
}

let heures = Number(prompt("Donnez-moi les heures à convertir en secondes : "));
let mins = Number(prompt("Donnez-moi les minutes à convertir : "));
let sec = Number(prompt("Donnez-moi les secondes de l'heure à convertir : "));

let tempsConverti = convert(heures, mins, sec);

console.log("Le temps converti en secondes est : " + tempsConverti + " secondes");