var prompt = require("prompt-sync")();

let adultes = Number(prompt("Donnez-moi le nombre d'adultes présents dans le foyer : "));
let enfants = Number(prompt("Donnez-moi le nombre d'enfants présents dans le foyer : "));
let revenusDepart = Number(prompt("Donnez-moi le montant de vos revenus nets imposables : "));

let tauxImposition1 = 11; 
let tauxImposition2 = 30; 
let tauxImposition3 = 41; 
let tauxImposition4 = 45; 

let palier1 = 10084;
let palier2 = 25710;
let palier3 = 73516;
let palier4 = 158122;

let part=0; 
let montantImpots = 0;


if (enfants>=3)
{
    part = adultes + enfants -1;
}
else
{
    part = adultes + enfants/2;
}

let revenus = revenusDepart / part;

switch (true) {
    case revenus>=10084 && revenus<25710:
        montantImpots = (revenus-10084) * (tauxImposition1/100);
        break;
    case revenus>=25710 && revenus<73516:
        montantImpots = (revenus-25710) * (tauxImposition2/100) + (palier2-palier1) * (tauxImposition1/100);
        break;
    case revenus>=73516 && revenus<158122:
        montantImpots = (revenus-73516) * (tauxImposition3/100) + (palier3-palier2) * (tauxImposition2/100) + (palier2-palier1) * (tauxImposition1/100);
        break;
    case revenus>=158122:
        montantImpots = (revenus-158122) * (tauxImposition4/100) + (palier4-palier3) * (tauxImposition3/100) + (palier3-palier2) * (tauxImposition2/100) + (palier2-palier1) * (tauxImposition1/100);
        break;
    default:
        montantImpots = 0;
        break;
}

montantImpots = montantImpots * part;

console.log("Le montant de l'impôt sur le revenu pour un foyer composé de " + adultes + " adulte(s) et de " + enfants + " enfant(s), avec un revenu fiscal de " + revenusDepart + " euros, s'élève à " + Math.round(montantImpots) + " euros");