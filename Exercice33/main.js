var prompt = require("prompt-sync")();

let longueur, T=[];

longueur = Number(prompt("Combien de valeurs comportent le tableau ? "));

for (let i = 0; i < longueur; i++) 
{
    T[i]= Number(prompt("Entrez la valeur " + (i+1) + " de votre tableau : "));
}

console.log("Voici votre tableau : " + T)

let min;

for (let i = 0; i < T.length; i++) 
{
    min = i;
    for (let j = i+1 ; j <= T.length; j++) 
    {
        if(T[j]<T[min])
        {
            min = j;
        }
    }
    if(min != i)
    {
        temp = T[i];
        T[i] = T[min];
        T[min] = temp;
    }
}

console.log("Votre tableau trié : " + T)