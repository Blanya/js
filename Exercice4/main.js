var prompt = require("prompt-sync")();

let word = prompt("Donnez-moi un mot: ");

//To reverse
function reverse(string)
{
    let rev_str = "";
    for( let i = string.length-1 ;i >= 0 ;i--)
    {
      rev_str += string[i];
    }
    // return reverse string
    return rev_str;
}

console.log("Vous avez saisi : " + word);
console.log("Le mot inversé est : " + reverse(word));

if(word == reverse(word))
{
    console.log("Le mot " + word + " est un palindrome")
}
else
{
    console.log("Le mot " + word + " n'est pas un palindrome")
}