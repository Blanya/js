var prompt = require("prompt-sync")();

let capitalInitial = Number(prompt("Donnez-moi le capital initial: "));
let interet = Number(prompt("Donnez-moi le taux d'intérêt: "));
let dureeEpargne = Number(prompt("Donnez-moi la durée d'Epargne: "));

let montantInterets = (capitalInitial * (Math.pow((1 + interet/100), dureeEpargne))) - capitalInitial;

console.log(`Avec un capital initial de ${capitalInitial}, placé à ${interet}% pendant ${dureeEpargne} année(s),`)
console.log("Le montant total des intérêts s'élevera à " + Math.round(montantInterets));
console.log("Le capital final à l'issue sera de " + Math.round(montantInterets + capitalInitial));