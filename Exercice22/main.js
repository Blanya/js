var prompt = require("prompt-sync")();

let nombre = Number(prompt("Donnez-moi un nombre : "));

while (nombre<10 || nombre>20) 
{
    if(nombre<10)
    {
        console.log("Plus grand");
    }
    else
    {
        console.log("Plus petit");
    }
    nombre = Number(prompt("Donnez-moi un nombre : "));
}