var prompt = require("prompt-sync")();

let age = Number(prompt("Donnez-moi votre age : "));
let salaire = Number(prompt("Donnez-moi votre dernier salaire : "));
let anciennete = Number(prompt("Donnez-moi votre ancienneté : "));

let indemnite = 0;

if(anciennete>=1 && anciennete<=10)
{
    indemnite = salaire/2 * anciennete;
}
else
{
    if(anciennete>10)
    {
        for(let i=0; i<anciennete; i++)
        {
            indemnite = indemnite + salaire;
        }    
    }
    else 
    {
        indemnite = 0;    
    } 
}

switch(true)
{
    case age>=46 && age<=49 :
        indemnite = indemnite + (salaire*2);
        break;
    case age>=50 :
        indemnite = indemnite + (salaire*5);
        break;
    default :
        indemnite = indemnite;
        break;
}

console.log("Le montant de l'indemnité pour un salarié de " + age + " ans avec " + anciennete + " années d'ancienneté s'élève à " + indemnite + " euros");
    