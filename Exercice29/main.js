var prompt = require("prompt-sync")();

let notes = [];

for (let i = 0; i < 20; i++) 
{
    notes[i] = Number(prompt("Saisie de la note " + (i+1) + " : "));
}

let moy, somme;
somme = 0;

for (let i = 0; i < notes.length; i++) 
{ 
    somme = somme + notes[i];
}

moy = somme/20 ;
console.log("La moyenne des notes est de : " + moy );

let plusGrande, plusPetite;

plusGrande = notes[0];
plusPetite = notes[0];

for (let i = 1; i < notes.length; i++) 
{
    if(notes[i] > plusGrande)
    {
        plusGrande = notes[i];
    }
    else if(notes[i] < plusPetite)
    {
        plusPetite = notes[i];
    }
}

console.log("La plus grande des notes est : " + plusGrande);
console.log("La plus petite des notes est : " + plusPetite);